/**
 * @file
 * Dialog API inspired by HTML5 dialog element.
 *
 * @see http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#the-dialog-element
 */

(function (Drupal) {
  /**
   * Creates a native HTML dialog element with specified content and settings.
   *
   * @param {string|HTMLElement} content - The content to be displayed in the dialog.
   * @param {Object} settings - Configuration settings for the dialog.
   * @param {string} settings.dialogClass - CSS classes to be added to the dialog.
   * @param {number|string} settings.width - Width of the dialog (in pixels or other CSS units).
   * @param {number|string} settings.height - Height of the dialog (in pixels or other CSS units).
   * @param {number|string} settings.minWidth - Minimum width of the dialog.
   * @param {number|string} settings.maxWidth - Maximum width of the dialog.
   * @param {number|string} settings.minHeight - Minimum height of the dialog.
   * @param {number|string} settings.maxHeight - Maximum height of the dialog.
   * @param {string} settings.resizable - Specify if the dialog is resizable.
   * @param {string} settings.position - Positioning of the dialog.
   * @param {string} settings.draggable - Specify if the dialog is draggable.
   * @param {boolean} settings.closeOnEscape - Whether to close the dialog on pressing the "Escape" key.
   * @param {Object} settings.classes - Additional CSS classes to be added to the dialog.
   * @param {string} settings.title - Title to be displayed in the dialog.
   * @param {Array<Object>} settings.buttons - Array of buttons to be added to the dialog.
   * @param {string} settings.buttons[].text - Text content of the button.
   * @param {string} settings.buttons[].class - CSS classes to be added to the button.
   * @param {function} settings.buttons[].click - Click event handler for the button.
   * @param {function} settings.close - Event handler for the dialog's close event.
   * @return {HTMLElement} - The created dialog container element.
   */
  function createDialog(content, settings) {
    const dialogContainer = document.createElement('dialog');
    // Add dialog classes.
    if (settings.dialogClass) {
      dialogContainer.classList.add(...settings.dialogClass.split(' '));
    }
    // TODO: Filter ui-dialog classes and avoid their attachment.
    if (settings.classes) {
      Object.entries(settings.classes).forEach(([key, value]) => {
        dialogContainer.classList.add(key, value);
      });
    }

    [
      'width',
      'height',
      'minWidth',
      'maxWidth',
      'minHeight',
      'maxHeight',
    ].forEach((dimension) => {
      if (settings[dimension]) {
        if (Number.isInteger(settings[dimension])) {
          dialogContainer.style[dimension] = `${settings[dimension]}px`;
        } else {
          dialogContainer.style[dimension] = settings[dimension];
        }
      }
    });

    if (settings.resizable) {
      dialogContainer.style.setProperty('resize', settings.resizable);
    }
    if (settings.position) {
      dialogContainer.style.setProperty('position', settings.position);
    }
    if (settings.draggable) {
      dialogContainer.setAttribute('draggable', settings.draggable);
    }
    if (settings.closeOnEscape === false) {
      dialogContainer.addEventListener('cancel', (event) => {
        event.preventDefault();
      });
    }

    dialogContainer.append(
      Drupal.theme('dialog', {
        content,
        ...settings,
      }),
    );

    // Add dialog event listener and attach to body.
    dialogContainer.addEventListener('close', settings.close);
    document.body.appendChild(dialogContainer);

    return dialogContainer;
  }

  /**
   * @typedef {object} Drupal.dialog~dialogDefinition
   *
   * @param {HTMLElement|string} element
   *   The HTML element / string to be inserted into the dialog container.
   * @param {object} settings
   *   The dialog options.
   * @return {HTMLElement} dialog
   *   The dialog element.
   */
  Drupal.dialogNative = function dialogNative(element, settings) {
    return createDialog(element, settings);
  };

  /**
   * Theme function for a dialog.
   *
   * @param {HTMLElement|string} content
   *   The content object.
   * @param {string} title
   *   The title text.
   * @param {object} buttons
   *   The buttons.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.dialog = ({ buttons, title, content }) => {
    const dialogWrapper = document.createElement('div');
    dialogWrapper.classList.add('dialog-native__wrapper');

    // Add dialog title.
    if (title) {
      dialogWrapper.append(Drupal.theme('dialogHeader', title));
    }

    // Add dialog content.
    if (content) {
      dialogWrapper.append(Drupal.theme('dialogContent', content));
    }

    // Add dialog buttons.
    if (buttons) {
      dialogWrapper.append(Drupal.theme('dialogButtons', buttons));
    }

    return dialogWrapper;
  };

  /**
   * Theme function for a dialog header.
   *
   * @param {string} title
   *   The title text.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.dialogHeader = (title) => {
    const dialogHeaderWrapper = document.createElement('div');
    dialogHeaderWrapper.classList.add('dialog-native__header');

    dialogHeaderWrapper.insertAdjacentHTML(
      'beforeend',
      `<h4 class="dialog-native__title">${title}</h4>`,
    );

    const button = document.createElement('button');
    button.innerText = Drupal.t('Close');
    button.classList.add('dialog-native__close');
    button.addEventListener('click', () => {
      button.dispatchEvent(
        new CustomEvent('close', {
          bubbles: true,
        }),
      );
    });
    dialogHeaderWrapper.append(button);

    return dialogHeaderWrapper;
  };

  /**
   * Theme function for the dialog content.
   *
   * @param {string|HTMLElement} content
   *   The dialog content.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.dialogContent = (content) => {
    const dialogContentWrapper = document.createElement('div');
    dialogContentWrapper.classList.add('dialog-native__content');

    if (typeof content === 'string') {
      dialogContentWrapper.insertAdjacentHTML('beforeend', content);
      // See drupal.ajax.js line 22, where an unnecessary "drupal-modal" wrapper
      // is created around our content. Remove it here:
    } else if (content.id === 'drupal-modal') {
      [...content.children].forEach((child) => {
        dialogContentWrapper.appendChild(child);
      });
    } else {
      dialogContentWrapper.appendChild(content);
    }
    return dialogContentWrapper;
  };

  /**
   * Theme function for a dialog buttons.
   *
   * @param {object} buttons
   *   The buttons.
   *
   * @return {HTMLElement}
   *   A DOM Node.
   */
  Drupal.theme.dialogButtons = (buttons) => {
    const dialogButtonsWrapper = document.createElement('div');
    dialogButtonsWrapper.classList.add('dialog-native__buttons');

    buttons.forEach((button) => {
      const buttonElement = document.createElement('button');
      buttonElement.textContent = button.text;
      buttonElement.classList.add(...button.class.split(' '));
      if (button.click) {
        buttonElement.addEventListener('click', button.click);
      }
      dialogButtonsWrapper.appendChild(buttonElement);
    });

    return dialogButtonsWrapper;
  };
})(Drupal);
