/**
 * @file
 * Dialog API inspired by HTML5 dialog element.
 *
 * @see http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#the-dialog-element
 */

class DrupalDialogEvent extends Event {
  constructor(type, dialog, settings = null) {
    super(`dialog:${type}`, { bubbles: true });
    this.dialog = dialog;
    this.settings = settings;
  }
}

(function ($, Drupal, drupalSettings) {
  /**
   * Default dialog options.
   *
   * @type {object}
   *
   * @prop {string} [dialogClass='']
   * @prop {string} [buttonClass='button']
   * @prop {string} [buttonPrimaryClass='button--primary']
   * @prop {function} close
   */
  drupalSettings.dialog = {
    dialogClass: 'dialog-native',
    // Drupal-specific extensions: see dialog.jquery-ui.js.
    buttonClass: 'button',
    buttonPrimaryClass: 'button--primary',
    // This is needed, so dialog-ajax.js can do its pseudo "close" code and
    // remove the dialog element after close:
    close(event) {
      Drupal.dialog(event.target).close();
      Drupal.detachBehaviors(event.target, null, 'unload');
    },
  };

  /**
   * Get an object containing only its properties (excluding methods).
   *
   * @param {Object} obj - The original object.
   * @return {Object} - An object containing only properties.
   */
  function _getObjectProperties(obj) {
    const result = {};
    Object.keys(obj).forEach((key) => {
      if (
        Object.prototype.hasOwnProperty.call(obj, key) &&
        typeof obj[key] !== 'function'
      ) {
        result[key] = obj[key];
      }
    });
    return result;
  }

  /**
   * Creates a dialog object using the provided element and options.
   *
   * The object interacts with a native HTML dialog element using jquery-ui
   * options and converting them to apply on the native dialog element.
   *
   * @param {HTMLElement} element
   *   The element that holds the dialog.
   * @param {object} options
   *   jQuery UI options to be passed to the dialog.
   *
   * @return {Drupal.dialog~dialogDefinition}
   *   The dialog instance.
   */
  Drupal.dialog = function (element, options) {
    const $element = $(element);
    const domElement = $element.get(0);

    const defaultDialogOptions = {
      open: false,
    };

    let dialog = { ...defaultDialogOptions, ...options };

    const futureImplementedOptions = [
      // A bit messy to implement currently:
      'autoOpen',
      // We currently have no close button for this:
      'closeText',
      // Won't be implemented:
      'show',
      // Might be implemented in the future:
      'hide',
    ];

    if (options) {
      const intersectedOptions = futureImplementedOptions.filter(
        function intersect(x) {
          return options[x] !== undefined;
        },
      );

      intersectedOptions.forEach((futureOption) => {
        console.log(`The "${futureOption}" option is currently not supported.`);
        delete options[futureOption];
      });

      if (options.appendTo) {
        Drupal.deprecationError({
          message: 'The "appendTo" option is deprecated and has no effect.',
        });
        console.log('The "appendTo" option is deprecated and has no effect.');
        delete options.appendTo;
      }
    }

    function openDialog(settings) {
      if (settings.dialogClass) {
        Drupal.deprecationError({
          message:
            'dialogClass is deprecated in drupal:10.4.x and will be removed from drupal:12.0.0.',
        });
      }
      // @todo Should dialog really overwrite the settings here, not the other
      // way around? We currently need it this way, so running dialog.option()
      // will have any effect.
      dialog = {
        ...drupalSettings.dialog,
        ...options,
        ...settings,
        ...dialog,
      };

      // Trigger a global event to allow scripts to bind events to the dialog.
      const event = new DrupalDialogEvent('beforecreate', dialog, settings);
      domElement.dispatchEvent(event);

      element = Drupal.dialogNative(element, _getObjectProperties(dialog));
      dialog.open = true;

      // Event listener on dialog close der dialog destroyed.
      element.addEventListener('close', () => {
        // @todo might not work, we might need to destroy on this, since this
        // fires at another point of time:
        dialog.destroy();
      });

      if (settings.modal) {
        element.showModal();
      } else {
        element.show();
      }
      domElement.dispatchEvent(
        new DrupalDialogEvent('aftercreate', dialog, settings),
      );
    }

    function closeDialog(value) {
      domElement.dispatchEvent(new DrupalDialogEvent('beforeclose', dialog));
      dialog.destroy();
      dialog.returnValue = value;
      dialog.open = false;
      domElement.dispatchEvent(new DrupalDialogEvent('afterclose', dialog));
    }

    dialog.show = () => {
      openDialog({ modal: false });
    };
    dialog.showModal = () => {
      openDialog({ modal: true });
    };

    // jquery Methods:
    dialog.destroy = () => {
      element.remove();
    };
    dialog.close = () => {
      closeDialog();
    };
    dialog.instance = () => {
      return element;
    };
    dialog.isOpen = () => {
      return dialog.open;
    };
    dialog.moveToTop = () => {
      Drupal.deprecationError({
        message: 'The "moveToTop" method is deprecated and has no effect.',
      });
      throw new Error(
        'The "moveToTop" method is deprecated and has no effect.',
      );
    };
    dialog.option = (optionName = 0, value = 0) => {
      if (optionName === 0 && value === 0) {
        return _getObjectProperties(dialog);
      }
      if (optionName !== 0 && value === 0) {
        if (dialog.hasOwnProperty(optionName)) {
          return dialog[optionName];
        }
        throw new Error(
          `The dialog object does not have an ${optionName} property.`,
        );
      }
      if (optionName !== 0 && value !== 0) {
        // Set the property on the dialog object:
        dialog[optionName] = value;
      }
    };
    // @todo Recreate all events listed in https://api.jqueryui.com/dialog/.
    return dialog;
  };
})(jQuery, Drupal, drupalSettings);
